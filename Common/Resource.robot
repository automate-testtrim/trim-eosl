*** Settings ***
Resource   ../Common/Keywords/CommonKeywords.robot
Resource   ../Common/Keywords/PricingKeywords.robot
Resource   ../Common/Keywords/SourcingKeywords.robot

Resource   ../Common/Variables/CommonVariables.robot
Resource   ../Common/Variables/PricingVariables.robot
Resource   ../Common/Variables/SourcingVariables.robot

Resource   ../Common/Repositories/CommonRepositories.robot
Resource   ../Common/Repositories/PricingRepositories.robot
Resource   ../Common/Repositories/SourcingRepositories.robot

