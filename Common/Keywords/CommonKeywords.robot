*** Settings ***
Library      SeleniumLibrary
Library      ScreenCapLibrary
Library      Screenshot
Resource     ../../Common/Resource.robot


*** Keywords ***
Login
    [Arguments]     ${username}       ${password}        
    Open Browser    ${url_sit}        ${browser}
    Maximize Browser Window
    Wait Element And Input Text             ${TextfieldUsername}    ${username}
    Wait Element And Input Text             ${TextfieldPassword}    ${password}
    Wait Element And Click Element          ${ButtonLogin}
    # Wait Element And Click Element          ${ForceLogout}
    Wait Element And Get Text For Verify    ${SystemMenu}[0]           ${TargetText}[0]         
    # Take Screenshot

Log Out
    Wait Element And Click Element          //*[@id="ui-accordion-main-menu-panel-0"]/div[1]/a
    Close All Browsers

# Select Menu
Verify Popup Confirmation
    [Arguments]    ${Action}
    Wait Until Keyword Succeeds    3x    2s    Page Should Contain Element    //div[@id='approve']//*[text()='Approve']
    Wait Until Page Contains Element    //*[text()='Are you sure you want to ${Action} this transaction?']
    CommonKeywords.Wait Element And Input Text         //*[@id='TextArea_Reason2']    Test Reason
    Sleep    3s
    # CommonKeywords.Wait Element And Click Element    ${ButtonYesApprove}

# Search.robot
# Read Excel
# Move File 

Wait Element And Input Text
    [Arguments]    ${locator}    ${text}
    Wait Until Page Contains Element	${locator}
    Input Text	${locator}	 ${text}

Wait Element And Click Element
    [Arguments]     ${locator}
    Wait Until Page Contains Element	${locator}
    Click Element	${locator}

Wait Element And Get Text
    [Arguments]     ${locator}
    Wait Until Page Contains Element    ${locator}
    ${text}    Get Text    ${locator}
    [Return]       ${text} 


Wait Element And Get Text For Verify
    [Arguments]           ${Locator}                      ${TargetText}
    ${SourceText}         Wait Element And Get Text       ${Locator}
    Should Be Equal       ${SourceText}                   ${TargetText}
    Log   ${SourceText} = ${TargetText}
    [Return]              ${SourceText}

Select Dropdown List
    [Arguments]    ${Field}    ${Value}
    Wait Element And Click Element        //select[contains(@id,'${Field}')]
    Wait Element And Click Element        //select[contains(@id,'${Field}')]//*[contains(text(),'${Value}')]

Select Dropdown Search
    [Arguments]    ${Field}    ${Value}
    Wait Element And Click Element       //*[contains(@id,'${Field}')]
    Wait Element And Input Text          //*[contains(@id,'${Field}')]/..//*[contains(@id,'Input_SearchTerm')]    ${Value}
    Press Keys                           //*[contains(@id,'${Field}')]/..//*[contains(@id,'Input_SearchTerm')]    ENTER

Select Dropdown Searchs
    [Arguments]    ${Field}    ${Value}
    Wait Element And Click Element       //*[contains(@id,'${Field}')]
    Wait Element And Input Text          //*[contains(@id,'${Field}')]/..//*[contains(@id,'Input_SearchTerm')]    ${Value}
    Press Keys                           //*[contains(@id,'${Field}')]/..//*[contains(@id,'Input_SearchTerm')]    ENTER

Open And Create Chrome Driver For Download
    [Arguments]    ${directoryFile}
    Set Global Variable    ${download_directory}    ${directoryFile}
    ${chrome_options}    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    ${prefs}    Create Dictionary    download.default_directory=${download_directory}    plugins.always_open_pdf_externally=${TRUE}
    Call Method    ${chrome_options}    add_experimental_option    prefs    ${prefs}
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Go To    https://rpachallenge.com/
    sleep    2s
    Maximize Browser Window
    sleep    2s
    Wait Element And Click Element    //a[text()=' Download Excel ']
    sleep    2s

