*** Settings ***
Library   RequestsLibrary
Library   OperatingSystem
Library   Collections

*** Variables ***
${API_URL}   https://236b2dea-89a5-4955-bed9-d250d3ef7a9d.mock.pstmn.io
${FILE_PATH}   D:\\Automate_TRIM\\trim-eosl\\TestDemo\\DataTestJson.xlsx

*** Test Cases ***
Upload XLSX File Test
    [Documentation]    Upload XLSX file with parameters and verify 201 response
    ${file}=   Get File For Streaming Upload   ${FILE_PATH}
    ${files}=   Create Dictionary   file=${file}   operator_format=false   Content-Type=multipart/form-data
    ${headers}=   Create Dictionary   Authorization=xxxxxxxxxxxxxxxxxxxxxxxxxxx   Accept=*/*    Content-Type=multipart/form-data    
    Create Session   mySession   ${API_URL}   verify=false
    ${response}=   POST On Session   mySession   /upload   headers=${headers}   data=${files}
    Should Be Equal As Strings    ${response.status_code}    201

    
